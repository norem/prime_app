<?php

Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function(){
   
    Route::get('index', 'DashboardController@index')->name('index');

     //users route
     Route::resource('users', 'UserController')->except('show');
     Route::post('users/upload_avator', 'UserController@uploadAvator');
     Route::get('users/export', 'UserController@export')->name('users.export');
     Route::post('users/usersimport', 'UserController@import')->name('users.import');
     Route::get('users/imports', 'UserController@loadImportView')->name('users.imports');

     //categotries route
     Route::resource('categories', 'CategoryController')->except('show');

     //Products route
     Route::resource('products', 'ProductController');

     //Dropdown
     Route::get('myform',array('as'=>'myform','uses'=>'ClientController@myform'));
     
     Route::get('districts/ajax/{id}',array('as'=>'district.ajax','uses'=>'ClientController@mydistrictformAjax'));
     Route::get('reports/export', 'ReportsController@export')->name('reports.export');
    //Clients route
    Route::resource('clients', 'ClientController')->except('show');
    Route::resource('clients.orders', 'Client\OrderController')->except('show');
    
    //orders route
    Route::resource('orders', 'OrderController');
    Route::get('/orders/{order}/products', 'OrderController@products')->name('orders.products');

    Route::resource('reports', 'ReportsController');

    //audits
    Route::get('/audits', 'AuditController@index')->name('audits.index');
    Route::get('/audits/export', 'AuditController@export')->name('audits.export');


});//end of dasboard routes