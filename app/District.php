<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $guarded = [];

    public function users(){
        return $this->hasMany(Client::class);
    }
}
