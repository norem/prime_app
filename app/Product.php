<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Product extends Model Implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];

    protected $appends = ['image_path', 'profit_percent'];

    public function getImagePathAttribute(){
        return asset('uploads/product_images/' . $this->image);
    }

    public function getProfitPercentAttribute(){
        return asset('uploads/product_images/' . $this->image);
    }

    public function category(){

        return $this->belongsTo(Category::class);
        
    }

    public function orders(){
        return $this->belongsToMany(Order::class);
    }
}
