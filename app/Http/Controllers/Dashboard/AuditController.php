<?php

namespace App\Http\Controllers\Dashboard;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuditController extends Controller
{
    public function index(){
        $product = Product::first();
        $audit = $product->audits()->latest()->first();

        return view('dashboard.audits.index', compact('audit'));
    }

    public function export(){

    }
}
