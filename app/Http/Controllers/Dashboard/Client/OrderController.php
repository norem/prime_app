<?php

namespace App\Http\Controllers\Dashboard\Client;

use App\Order;
use App\Client;
use App\Product;
use App\Category;
use Illuminate\Http\Request;    
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(){
        
    }

    public function create(Client $client){

        $categories = Category::with('products')->get();

        return view('dashboard.clients.orders.create', compact('client', 'categories'));
    }

    public function store(Request $request, Client $client){

        $request->validate([
            'products' => 'required|array',
            'payment_mode' => 'required',
            'point_of_sale' => 'required'
        ]);
        
        $order = $client->orders()->create([
            'payment_mode' => $request->payment_mode,
            'point_of_sale' => $request->point_of_sale
        ]);
        
        $order->products()->attach($request->products);

        $total_price = 0;

        foreach($request->products as $id=>$quantity){

            $product = Product::FindOrFail($id);

            if($product->sales_price >= $product->initial_price){}

            $total_price += $product->sales_price * $quantity['quantity'];
            
            $product->update([
                'stock' => $product->stock - $quantity['quantity'],
            ]);
        }//end of foreach

        $order->update([
            'total_price' => $total_price,
        ]);
        

        notify()->success('Order was created successfully!');

        return redirect()->route('dashboard.orders.index');
    }

    public function edit(Client $client, Order $order){
        
    }

    public function update(Request $request, Client $client, Order $order){
        
    }

    public function destroy(Client $client, Order $order){
        
    }
}
