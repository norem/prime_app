<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use App\Client;
use App\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $currentUser = Auth::user();
        
        $clients = Client::where('user_id', $currentUser->id)->paginate(10);

        return view('dashboard.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = Region::with('districts')->pluck("name","id");
        return view('dashboard.clients.create', compact('regions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name' => 'required',
            'gender' => 'required',
            'age_group' => 'required',
            'phone' => 'required',
            'place' => 'required',
            'region_id' =>  'required',
            'district_id' => 'required',
            'address' => 'required',
            'client_type' => 'required',

        ]);

        Client::create($request->all());

        notify()->success('Client created successfully!');

        return redirect()->route('dashboard.clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $clientsInfo = Client::get();
        return view('dashboard.clients.edit', compact('client', 'clientsInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();
    }

    public function mydistrictformAjax($id) {

        $districts = DB::table("districts")

                    ->where("region_id",$id)

                    ->pluck("name","id");

        return json_encode($districts);
    }

    public function mydivisionformAjax($id) {

        $divisions = DB::table("divisions")

                    ->where("district_id",$id)

                    ->pluck("name","id");

        return json_encode($divisions);
    }

}
