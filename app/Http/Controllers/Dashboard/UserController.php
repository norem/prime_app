<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{

    public function __construct(){
        $this->middleware(['permission:read-users'])->only('index');
        $this->middleware(['permission:create-users'])->only('create');
        $this->middleware(['permission:update-users'])->only('edit');
        $this->middleware(['permission:delete-users'])->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        
        $users = User::when($request->search, function($query) use ($request){

                return $query->where('first_name', 'like', '%' . $request->search . '%')
                ->orWhere('last_name', 'like', '%' . $request->search . '%')
                ->orWhere('email', 'like', '%' . $request->search . '%');
    
        })->latest()->paginate(10);
        
        

        return view('dashboard.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $managers = User::get();
        return view('dashboard.users.create', compact('managers'));
    }

    public function uploadAvator(Request $request){
        dd('worked');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'password' => 'required|confirmed',
            
        ]);

        $request_data = $request->except(['password', 'password_confirmation', 'permissions', 'avator']);

        if($request->avator){
            Image::make($request->avator)->resize(300, null, function($constrain){
                $constrain->aspectRatio();
            })->save(public_path('/uploads/usersImage/' .$request->avator->hashName()));

            $request_data['avator'] = $request->avator->hashName();

        } //end of if
        
        $request_data['password'] = bcrypt($request->password);

        $user = User::create($request_data);

        $user->attachRole('admin');

        $user->syncPermissions($request->permissions);

        
        notify()->success('User created successfully');
        return redirect()->route('dashboard.users.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $employees = User::all();
        return view('dashboard.users.edit', compact('user', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        // dd($request->all());
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', Rule::unique('users')->ignore($user->id),],
        ]);

        $request_data = $request->except(['permissions', 'avator']);

        if($request->avator){

            if($user->avator != 'default.png'){
                Storage::disk('public_uploads')->delete('usersImage/' . $user->avator);
            }

           Image::make($request->avator)->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('uploads/usersImage/' . $request->avator->hashName()));

        $request_data['avator'] = $request->avator->hashName();

       }//end if statement

        $user->update($request_data);

        $user->syncPermissions($request->permissions);

        notify()->success('User updated successfully');
        return redirect()->route('dashboard.users.index');
    }

    public function export(){
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    public function import(Request $request){

        $users = Excel::import(new UsersImport, $request->file('import_users'));
        notify()->success('Users imported successfully');
        return redirect()->route('dashboard.users.index');
    }

    public function loadImportView(){
        return view('dashboard.users.imports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if($user->avator != 'default.png'){
            Storage::disk('public_uploads')->delete('usersImage/' . $user->avator);
        }

        $user->delete();
        notify()->success('User deleted succesfully');

        return redirect()->route('dashboard.users.index');
    }
}
