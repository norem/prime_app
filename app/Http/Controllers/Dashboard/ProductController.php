<?php

namespace App\Http\Controllers\Dashboard;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::all();

        $products = Product::when($request->product_search, function($query) use ($request){

            return $query->where('name', 'like', '%' . $request->product_search . '%');

        })->when($request->category_id, function ($q) use ($request){
           
            return $q->where('category_id', $request->category_id);

        })->latest()->paginate(10);

        return view('dashboard.products.index', compact('categories','products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('dashboard.products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'category_id' => 'required',
            'name' => 'required|unique:products,name',
            'description' => 'required',
            'sales_price' => 'required',
            'stock' => 'required',

    ];

        $request->validate($rules); 

        $request_data = $request->all();

        
           if($request->image){
               Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/product_images/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

           }//end if statement

           Product::create($request_data);

           notify()->success('Product created succesfully');

           return redirect()->route('dashboard.products.index');
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('dashboard.products.edit', compact(['categories', 'product']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $rules = [
            'category_id' => 'required',
            'name' => 'required|unique:products,name,' . $product->id,
            'description' => 'required',
            'sales_price' => 'required',
            'stock' => 'required',
    ];

        $request->validate($rules); 

        $request_data = $request->all();

        
           if($request->image){

                if($product->image != 'default.png'){
                    Storage::disk('public_uploads')->delete('product_images/' . $product->image);
                }

               Image::make($request->image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save(public_path('uploads/product_images/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

           }//end if statement

           $product->update($request_data);

           notify()->success('Product updated succesfully');

           return redirect()->route('dashboard.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if($product->image != 'default.png'){
            Storage::disk('public_uploads')->delete('product_images/' . $product->image);
        }

        $product->delete();

        notify()->success('Product deleted succesfully');

        return redirect()->route('dashboard.products.index');
    }
}
