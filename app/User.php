<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email','manager_id', 'password', 'avator',
    ];

    protected $appends = ['full_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute(){
        return "{$this->first_name} {$this->last_name}";
    }

    public function clients(){
        return $this->hasMany(Client::class);
    }

    public function manager(){
        return self::where('id', $this->manager_id)->get();
    }

    public function getManagerNameAttribute(){
        // dd($this->manager()->first()->full_name);
        return $this->manager()->first()['full_name'];
    }

    // public function managees(){
    //     return $this->belongsTo(self::class, 'manager_id', 'id');
    // }
}
