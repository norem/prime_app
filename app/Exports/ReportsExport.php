<?php

namespace App\Exports;

use App\Order;
use App\Client;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReportsExport implements FromView
{
        
    public function view(): View
    {
        return view('dashboard.reports.table', [
            'clients' => Client::all(),
            'orders' => Order::all(),
        ]);
    }
}
