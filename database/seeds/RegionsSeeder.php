<?php

use Illuminate\Database\Seeder;

class RegionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Region::create([
            'name' => 'central region'
        ]);
        App\Region::create([
            'name' => 'eastern region'
        ]);
        App\Region::create([
            'name' => 'western region'
        ]);
        App\Region::create([
            'name' => 'northern region'
        ]);
    }
}
