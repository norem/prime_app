<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Product::create([
            'category_id' => 1,
            'name' => 'Product A',
            'description' => 'This is Product A',
            'initial_price' => 12000,
            'sales_price' => 15000,
            'stock' => 100,
        ]);

        App\Product::create([
            'category_id' => 2,
            'name' => 'Product B',
            'description' => 'This is Product B',
            'initial_price' => 30000,
            'sales_price' => 35000,
            'stock' => 100,
        ]);
    }
}
