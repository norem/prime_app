<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory(App\Client::class, 50)->create()->each(function (Faker $faker) {
    $client->orders()->save(factory(App\Order::class)->make());
});
