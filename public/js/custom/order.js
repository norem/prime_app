$(document).ready(function(){
    let discount = $('#discount');
    $('.add-product-btn').on('click', function(e){

        e.preventDefault();
        let name = $(this).data('name');
        let id = $(this).data('id');
        let price = $(this).data('sales_price');
        

        $(this).removeClass('btn-success').addClass('btn-default disabled');

        let renderHtml = 
        `<tr>
            <td>${name}</td>
            <td>In stock</td>
            <td><input type="number" name="products[${id}][quantity]" data-sales_price="${price}" class="form-control product-quantity" min="1" value="1"></td>
            <td class="product-price">${price}</td>
            <td class="discount"><input type="number" name="" step="0.01" id="discount"></td>
            <td><button class="btn btn-sm btn-danger remove-product-btn" data-id="${id}"><i class="fa fa-trash"></i></button></td>
        </tr>`;

        $('.order-list').prepend(renderHtml);

        calculateTotal();
    });

    $('body').on('click', '.remove-product-btn', function(e){
        e.preventDefault();

        let id = $(this).data('id');

        $(this).closest('tr').remove();
        
        $('#product-' + id).removeClass('btn-default disabled').addClass('btn-success');

        calculateTotal();
    }); //end of remove product btn


    $('body').on('keyup change', '.product-quantity', function(){
        let quantity = parseInt($(this).val());
        let sales_price = $(this).data('sales_price');
        let totalPrice = $.number(quantity * sales_price,2);
        $(this).closest('tr').find('.product-price').html(totalPrice);
        
        calculateTotal();
    });//end of product quantity change

    $('.order-products').on('click', function(e){
        e.preventDefault();

        let url = $(this).data('url');
        let method = $(this).data('method');
        
        $.ajax({
            url: url,
            method: method,

            success: function(data){
                $('#order-products-show').empty();
                $('#order-products-show').append(data);
            }

        })
    });//end of order-products btn

}); // end of $(document)

function calculateTotal(){
        
        let price = 0;

        $('.order-list .product-price').each(function(index){

            price += parseFloat($(this).html().replace(/,/g,''));
            
        });

        $('.total-price').html($.number(price,2));

        if(price > 0){

            $('#place-order-btn').removeClass('disabled');
        }else{
            $('#place-order-btn').addClass('disabled');
        }
    }