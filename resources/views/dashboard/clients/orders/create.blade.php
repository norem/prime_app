@extends('layouts.dashboard.app')
<style>
    .clickable{
    cursor: pointer;   
}

.panel-heading span {
	margin-top: -20px;
	font-size: 15px;
}

.panel-heading a{
    text-decoration: none;
    color: #000;
}
</style>
@section('content')
<div class="container">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create Order</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
                    <li class="breadcrumb-item"><a href="#">Orders</a></li>
                    <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    
    <div class="container">
        <div class="row">
            <div class="col-10 offset-1">
                <div class="card card-primary">

                    @foreach ($categories as $category)
                        <div class="panel-group">
                            <div class="col-md-6">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            <a data-toggle="collapse" href="#{{ str_replace(' ', '-', $category->name )}}">{{ $category->name }}</a>
                                        </h3>
                                        <span class="pull-right clickable "><i class="fa fa-arrow-up"></i></span>
                                    </div>
                                    <div class="panel-body">
                                        @if ($category->products->count() > 0)
                                            <table class="table table-hover">
                                                <tr>
                                                    <th>Product Image</th>
                                                    <th>Name</th>
                                                    <th>Stock</th>
                                                    <th>Price</th>
                                                    <th>Order</th>
                                                </tr>

                                                @foreach ($category->products as $product)
                                                    <tr>
                                                        <td><img src="{{ asset('uploads/product_images/' .$product->image)}}" alt="" width="100"></td>
                                                        <td>{{$product->name}}</td>
                                                        <td>{{$product->stock}}</td>
                                                        <td>{{$product->sales_price}}</td>
                                                        <td>
                                                            <a href="" id="product-{{$product->id}}" 
                                                            data-name="{{$product->name}}"
                                                            data-id="{{$product->id}}"
                                                            data-sales_price="{{$product->sales_price}}"
                                                            class="btn btn-success btn-sm add-product-btn">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                            
                                            </table><!-- / table -->
                                            @else 
                                            <h3>No Products found</h3>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div><!-- ./panel-group -->
                    @endforeach
                
                    <div class="container mb-4">
                        <form action="{{route('dashboard.clients.orders.store', $client->id)}}" method="post">
                        @csrf
                        @method('POST')
                        <div class="row">
                            <div class="col-12">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">Product</th>
                                                <th scope="col">Available</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Price</th>
                                                <th scope="col">Discount</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="order-list">
                                            
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><strong>Total</strong></td>
                                                <td class="text-right total-price"></td>
                                            </tr>
                                            <tr style="width: 300px">
                                                <label for="pos">Point of Sale</label>
                                                <select name="point_of_sale" class="form-control" required >
                                                    <option value="branch a">branch A</option>
                                                    <option value="branch b">Branch B</option>
                                                </select>
                                            </tr>
                                                <tr>
                                                    <label for="payment_mode">Payment Mode</label>
                                                    <select name="payment_mode" class="form-control" required>
                                                        <option value="cash">Cash</option>
                                                        <option value="paygo">PayGo</option>
                                                        <option value="credit">Credit</option>
                                                    </select>
                                                </tr>
                                            <tr></tr>
                                            <tr></tr>
                                            <tr></tr>
                                            <tr></tr>
                                        </tbody>
                                    </table><!-- end of table -->
                                </div>
                            </div>
                            <div class="col mb-2">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 text-right">
                                        <button class="btn btn-lg btn-block btn-success text-uppercase disabled" id="place-order-btn">Checkout</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </form><!-- end of form -->
                        
                    </div><!-- end of shopping cart -->

                </div>
            </div>
        </div>
    </div><!-- ./cart container -->

</div><!-- Container end -->
    

@endsection

@section('orderjs')
<script>
 $(document).ready(function(){
        $('.panel').find('.panel-body').slideUp();
        $(document).on('click', '.panel-heading span.clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('fa-arrow-up').addClass('fa-arrow-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('fa-arrow-down').addClass('fa-arrow-up');
        }
    });
 });   
	


</script>
<script src="{{ asset('js/custom/order.js') }}"></script>
@endsection