@extends('layouts.dashboard.app')

@section('content')
<div class="container">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">{{ $category->name}} - Category</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
                    <li class="breadcrumb-item"><a href="#">Categories</a></li>
                    <li class="breadcrumb-item active">Update</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

   
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Update Category Details</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
  <form action="{{ route('dashboard.categories.update', $category->id) }}" method="post">
    @csrf
    @method('PUT')


      <div class="card-body">
        <div class="form-group">
          <label>Name</label>
        <input type="text" name="name" class="form-control"  value="{{ $category->name }}" >
        </div>

        <div class="form-group">
            <label>Description</label>
            <input type="text" name="description" class="form-control" value="{{ $category->description }}" >
        </div>

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update Category <i class="fa fa-plus"></i></button>
    </div>
</form>

</div>
@endsection