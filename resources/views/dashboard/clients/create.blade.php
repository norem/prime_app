@extends('layouts.dashboard.app')

@section('content')
<div class="container">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create Client</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
                    <li class="breadcrumb-item"><a href="#">clients</a></li>
                    <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


<div class="container">
    <div class="row">
        
        <div class="col-10 offset-1">
            <div class="card card-primary">
  
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('dashboard.clients.store') }}" method="post">
                    @csrf
                    @method('post')

                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">

                    <div class="card-body">
                        <div class="form-group">
                        <label>Name</label>
                            <input type="text" name="name" class="form-control"  value="{{ old('name') }}" >
                        </div>
                        
                        <div class="form-group">
                        <label>Gender</label>
                        <select name="gender" class="form-control" >
                            <option value="" selected disabled>Select Gender</option>
                            <option value="male">Male</option>
                            <option value="female">Female</option>
                        </select>
                        </div>

                        <div class="form-group">
                            <label>Age Group</label>
                            <select name="age_group" class="form-control" >
                                <option value="" selected disabled>Select Age Group</option>
                                <option value="17-35">17 - 35</option>
                                <option value="36-53">36 - 53</option>
                                <option value="54-71">54 - 71</option>
                                <option value="72-89">72 - 89</option>
                                <option value="unknown">Unknown</option>
                            </select>
                        </div>

                        <div class="form-group">
                        <label>Phone (optional)</label>
                        <input type="text" name="phone" class="form-control"  value="{{ old('phone') }}" >
                        </div>

                        <div class="form-group">
                        <label>Place</label>
                            <select name="place" class="form-control" >
                                <option value="" selected disabled>Select Place</option>
                                <option value="urban">Urban</option>
                                <option value="rural">Rural</option>
                            </select>
                        </div>

                        <address class="">
                            <div class="form-group">
                                <label for="title">Select Region:</label>
                                <select id="region" name="region_id" class="form-control" >
                                    <option value="" selected disabled>Select Region</option>
                                    @foreach($regions as $key => $region)
                                        <option value="{{$key}}"> {{$region}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Select District:</label>
                                <select name="district_id" id="district" class="form-control">
                                </select>
                                <img id="loader" src="{{ asset('uploads/loader50.gif')}}" alt="" srcset="">
                            </div>
                        
                            <div class="form-group">
                                <label>Physical Address</label>
                                <input name="address" class="form-control" value="{{ old('address') }}">
                            </div>
                        </address>

                        <div class="form-group">
                            <label>Client Type</label>
                            <select name="client_type"  class="form-control" >
                                <option value="" selected disabled>Select Client Type</option>
                                <option value="household">Household</option>
                                <option value="business">Business</option>
                                <option value="institution">Institution</option>
                                <option value="farm">Farm</option>                            
                                <option value="">Other</option>                            
                            </select>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Create Client <i class="fa fa-plus"></i></button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
        
</div>
    

@endsection

@section('orderjs')
<script type="text/javascript">

    $(document).ready(function() {

        $('select[name="region_id"]').on('change', function() {

            var regionID = $(this).val();
            
            if(regionID) {

                $.ajax({

                    url: '/dashboard/districts/ajax/'+regionID,

                    type: "GET",

                    dataType: "json",

                    beforeSend: function(){
                        $('#loader').css("visibility", "visible");
                    },
                    
                    success:function(data) {

                        $('select[name="district_id"]').empty();

                        $.each(data, function(key, value) {
                            
                            $('select[name="district_id"]').append('<option value="'+ key +'">'+ value +'</option>');

                        });
                    },

                    complete: function(){
                        $('#loader').css("visibility", "hidden");
                     }

                });

            }else{

                $('select[name="district_id"]').empty();

            }

        });

    });

</script>
@endsection
