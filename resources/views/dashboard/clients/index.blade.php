@extends('layouts.dashboard.app')

@section('content')
<div class="container">
  


    <div class="content-header">
        <div class="container-fluid">
          
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">client</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">client</a></li>
                        <li class="breadcrumb-item active">Index</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="card">

        


        <div class="card-header" >
        <h3 class="card-title">All clients</h3>
          @if (auth()->user()->hasPermission('create-clients'))
            <a href="{{ route('dashboard.clients.create') }}"  class="btn btn-primary float-right">Create a client <i class="fa fa-plus "></i></a>
          @endif
      </div>
        
        <!-- /.card-header -->
        <div class="card-body">
          <!-- Search form -->
          <form action="{{ route('dashboard.clients.index')}}" method="get" class="mb-4">
            <div class="input-group">
            <input type="text" class="form-control" name="client_search" value="{{ request()->client_search}}" placeholder="Looking for client!!">
              <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </div>
            </div>
          </form>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Age Group</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Created By</th>
                <th>Orders</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody id="clientsTable">
                @foreach ($clients as $index=>$client)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->gender }}</td>
                        <td>{{ $client->age_group }}</td>
                        <td>{{ $client->phone }}</td>  
                        <td>{{ $client->region->name }}, {{ $client->district->name }}, {{ $client->address }}</td>
                        <td>{{ $client->user->full_name }}</td>
                        <td>
                          @if (auth()->user()->hasPermission('create-orders'))
                            <a href="{{ route('dashboard.clients.orders.create', $client->id)}}" class="btn btn-primary btn-block btn-sm">Place Order</a>
                          @else
                          <a href="#" class="btn btn-default btn-block btn-sm" disabled>Place Order</a>
                          @endif
                        </td>
                        <td>
                          @if (auth()->user()->hasPermission('update-clients'))
                            <a href="{{ route('dashboard.clients.edit', $client->id) }}" class="btn btn-info btn-sm">Edit</a>
                          @endif
                        
                        <form action="{{ route('dashboard.clients.destroy', $client->id) }}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                             @if (auth()->user()->hasPermission('delete-clients'))
                              <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                             @endif
                            
                        </form>
                        </td>
                    </tr>
                @endforeach
              
            </tbody>
          </table><!-- end of table -->
        </div><!-- end of table responsive wrapper -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{ $clients->appends(request()->query())->links()}}
        </div>
      </div>

    
</div>
@endsection