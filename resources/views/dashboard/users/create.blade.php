@extends('layouts.dashboard.app')

@section('content')
<div class="container">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create User</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
                    <li class="breadcrumb-item"><a href="#">Users</a></li>
                    <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


<div class="container">
    <div class="row">
        
        <div class="col-10 offset-1">
            <div class="card card-primary">
  
                <!-- /.card-header -->
                <!-- form start -->
            <form action="{{ route('dashboard.users.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')


                  <div class="card-body">
                    <div class="form-group">
                      <label>First Name</label>
                    <input type="text" name="first_name" class="form-control"  value="{{ old('first_name') }}" >
                    </div>
            
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control" value="{{ old('last_name') }}" >
                    </div>
            
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}" >
                    </div>

                    <div class="form-group">
                        <label>Line Manager</label>
                        <select name="manager_id" class="form-control">
                          <option value="" selected disabled>Select Manager</option>
                          @foreach ($managers as $manager)
                            <option value="{{$manager->full_name}}" >{{$manager->full_name}}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Password Confirmation</label>
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>User Image</label>
                        <input type="file" name="avator" class="form-control">
                    </div>

                    <hr>


                    @php
                        $models = ['users', 'categories', 'products', 'Clients', 'Orders'];
                        $maps = ['create', 'read', 'update', 'delete'];
                      @endphp
                        
                      <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        @foreach ($models as $index=>$model)
                        <li class="nav-item m-1">
                          <a class="nav-link {{ $index == 0 ? 'active' : ''}}" id="pills-{{$model}}-tab" data-toggle="pill" href="#pills-{{$model}}" role="tab" aria-controls="pills-{{$model}}" aria-selected="true">{{$model}}</a>
                        </li>
                        @endforeach
                      </ul>
                      <div class="tab-content" id="pills-tabContent">
                        @foreach ($models as $index=>$model)
                        <div class="tab-pane fade show {{ $index == 0 ? 'active' : ''}}" id="pills-{{$model}}" role="tabpanel" aria-labelledby="pills-{{$model}}-tab">
                          @foreach ($maps as $index=>$map)
                          <div class="form-check d-inline-flex">
                            <input type="checkbox" name="permissions[]" value="{{ $map . '-' . $model}}" class="form-check-input">
                            <label class="form-check-label" for="{{ $map }}">{{ $map }}</label>
                          </div>
                          @endforeach
                        </div>
                        @endforeach
                      </div>
              
                  </div>
                  <!-- /.card-body -->

              
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create User <i class="fa fa-plus"></i></button>
                </div>
            </form>
              </div>
            
        </div>
    </div>
        
</div>

</div><!-- Container end -->
    

@endsection