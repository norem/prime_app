@extends('layouts.dashboard.app')

@section('content')
<div class="container">
  
    <div class="content-header">
        <div class="container-fluid">
          
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Users</a></li>
                        <li class="breadcrumb-item active">Imports</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="container">
      <div class="card">

        <div class="card-body">
          <div class="col-md-6 col-sm-6">
            <form action="{{route('dashboard.users.import')}}" method="POST" enctype="multipart/form-data" class="form-group">
              @csrf
              <input id="usersUpload" name="import_users"  type="file">
            </form>
          </div>
        </div>
      </div>
  </div><!-- /.container -->
</div>
@endsection

@section('orderjs')
<script>
  $(document).ready(function() {
    $("#usersUpload").fileinput({showCaption: false, dropZoneEnabled: false});
});
</script>
@endsection