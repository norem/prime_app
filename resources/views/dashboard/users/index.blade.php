@extends('layouts.dashboard.app')

@section('content')
<div class="container">
  
    <div class="content-header">
        <div class="container-fluid">
          
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Users</a></li>
                        <li class="breadcrumb-item active">Index</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="card">

        <div class="card-header" >
        <h3 class="card-title">All Users  -  {{ $users->total()}}</h3>
        <br>
        <br>
        <div class="row">
            <div class="col-md-6 col-sm-6">
              <a href="{{ route('dashboard.users.imports') }}" class="btn btn-warning">Import Users</a>              
            </div>
            <div class="col-md-6 col-sm-6">
              <a href="{{ route('dashboard.users.export') }}"  class="btn btn-primary float-right ml-1">Download Users xlsx <i class="fa fa-file-excel-o "></i></a>

              @if (auth()->user()->hasPermission('create-users'))
                <a href="{{ route('dashboard.users.create') }}"  class="btn btn-primary float-right">Create a User <i class="fa fa-plus "></i></a>
              @endif
            </div>
        </div><!-- /.row -->
        
      </div>
        
        <!-- /.card-header -->
        <div class="card-body">
          <!-- Search form -->
          <form action="{{ route('dashboard.users.index')}}" method="get" class="mb-4">
            <div class="input-group">
            <input type="text" class="form-control" name="search" value="{{ request()->search}}" placeholder="Looking for someone!!">
              <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </div>
            </div>
          </form>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th style="width: 120px">Photo</th>
                <th style="width: 180px">Full Name</th>
                <th style="width: 250px">Email</th>
                <th style="width: 200px">Line Manager</th>
                <th style="width: 130px">Actions</th>
              </tr>
            </thead>
            <tbody id="usersTable">
                @foreach ($users as $index=>$user)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>
                          @if ($user->avator)
                          <img src="{{ asset('uploads/usersImage/' . $user->avator ) }}" alt="" class="img-thumbnail" width="300">
                            @else
                            <strong>NO PHOTO</strong>
                          @endif
                        </td>
                        <td>{{ $user->full_name}}</td>
                        <td>{{ $user->email}}</td>
                        <td>
                          
                            {{$user->managerName}}
                         
                        </td>                        
                        <td>
                          @if (auth()->user()->hasPermission('update-users'))
                            <a href="{{ route('dashboard.users.edit', $user->id) }}" class="btn btn-info btn-sm">Edit</a>
                          @endif
                        
                        <form action="{{ route('dashboard.users.destroy', $user->id) }}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                             @if (auth()->user()->hasPermission('delete-users'))
                              <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                             @endif
                            
                        </form>
                        </td>
                    </tr>
                @endforeach
              
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{$users->appends(request()->query())->links()}}
        </div>
      </div>

    
</div>
@endsection
