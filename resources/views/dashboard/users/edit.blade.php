@extends('layouts.dashboard.app')

@section('content')
<div class="container">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
                    <li class="breadcrumb-item"><a href="#">Users</a></li>
                    <li class="breadcrumb-item active">Update</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

   
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Update User Details</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
  <form action="{{ route('dashboard.users.update', $user->id) }}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')


      <div class="card-body">
        <div class="form-group">
          <label>First Name</label>
        <input type="text" name="first_name" class="form-control"  value="{{ $user->first_name }}" >
        </div>

        <div class="form-group">
            <label>Last Name</label>
            <input type="text" name="last_name" class="form-control" value="{{ $user->last_name }}" >
        </div>

        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="{{ $user->email }}" >
        </div>
        
        <div class="form-group">
          <label>Line Manager</label>
          <select name="manager_id" class="form-control">
            <option value="">None</option>
              @foreach ($employees as $employee)
                <option value="{{$employee->id}}" {{$user->manager_id == $employee->id ? 'selected' : ''}} > {{$employee->full_name}} </option>
              @endforeach
          </select>
        </div>
        <div class="form-group">
          <label>User Image</label>
          <input type="file" name="avator" class="form-control">
      </div>

      <div class="form-group">
        <img src="{{ asset('uploads/usersImage/' . $user->avator ) }}" alt="" class="img-thumbnail" width="100">
      </div>
        <hr>
        
        @php
        $models = ['users'];
        $maps = ['create', 'read', 'update', 'delete'];
      @endphp
        
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
          @foreach ($models as $index=>$model)
          <li class="nav-item m-1">
            <a class="nav-link {{ $index == 0 ? 'active' : ''}}" id="pills-{{$model}}-tab" data-toggle="pill" href="#pills-{{$model}}" role="tab" aria-controls="pills-{{$model}}" aria-selected="true">{{$model}}</a>
          </li>
          @endforeach
        </ul>
        <div class="tab-content" id="pills-tabContent">
          @foreach ($models as $index=>$model)
          <div class="tab-pane fade show {{ $index == 0 ? 'active' : ''}}" id="pills-{{$model}}" role="tabpanel" aria-labelledby="pills-{{$model}}-tab">
            @foreach ($maps as $index=>$map)
            <div class="form-check d-inline-flex">
              <input type="checkbox" name="permissions[]" {{ $user->hasPermission($map . '-' . $model) ? 'checked' : ''}} value="{{ $map . '-' . $model}}" class="form-check-input">
              <label class="form-check-label" for="{{ $map }}">{{ $map }}</label>
            </div>
            @endforeach
          </div>
          @endforeach
        </div>
      </div>
      <!-- /.card-body -->

    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update User  <i class="fa fa-refresh"></i></button>
    </div>
</form>
</div>
</div>
@endsection