@extends('layouts.dashboard.app')

@section('content')
<div class="container">

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>{{$orders_count}}</h3>
  
                  <p>Total Orders</p>
                </div>
                <div class="icon">
                  <i class="fa fa-shopping-basket"></i>
                </div>
                <a href="{{ route('dashboard.orders.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                <h3>{{$products_count}}</h3>
  
                  <p>Total Products</p>
                </div>
                <div class="icon">
                  <i class="fa fa-product-hunt"></i>
                </div>
                <a href="{{ route('dashboard.products.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{$users_count}}</h3>
  
                  <p>Total Users</p>
                </div>
                <div class="icon">
                  <i class="fa fa-user-circle"></i>
                </div>
                <a href="{{ route('dashboard.users.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                <h3>{{$clients_count}}</h3>
  
                  <p>Total Clients</p>
                </div>
                <div class="icon">
                  <i class="fa fa-users"></i>
                </div>
                <a href="{{ route('dashboard.clients.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div> <!-- /.row -->

          <div class="card">
              <div class="card-heading">
                  <h3 class="card-title chart-title">
                    Revenue Charts
                  </h3>
              </div>
              <div class="card-body">
                <div class="chart" id="line-chart"></div>
              </div>
          </div>
          
          
          <!-- /.card -->
          <!-- Main row -->
        </section>
</div>

@endsection


@section('morris')
<script>
   new Morris.Line({
  // ID of the element in which to draw the chart.
  element: 'line-chart',
  resize: true,
  data: [
    @foreach($sales_data as $data)
    {
        ym: "{{ $data->year }}-{{ $data->month }}",
        sum: "{{ $data->sum }}"
    } @endforeach,
  ],
   
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    
    // The name of the data record attribute that contains x-values.
    xkey: 'ym',
    // A list of names of data record attributes that contain y-values.
    ykeys: ['sum'],
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Total'],
    lineWidth: 2,
    hideHover: 'auto',
    gridStrokeWidth: 0.4,
    pointSize: 4,
    gridTextFamily: 'open sans',
    gridTextSize: 10    
});
    
</script>
@endsection



                    
               