@extends('layouts.dashboard.app')

@section('content')
<div class="container">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create Product</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
                    <li class="breadcrumb-item"><a href="#">Products</a></li>
                    <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


<div class="container">
    <div class="row">
        
        <div class="col-10 offset-1">
            <div class="card card-primary">
  
                <!-- /.card-header -->
                <!-- form start -->
            <form action="{{ route('dashboard.products.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('post')


                  <div class="card-body">
                    <div class="form-group">
                      <label>Name</label>
                    <input type="text" name="name" class="form-control"  value="{{ old('name') }}" >
                    </div>
                    
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class=" form-control ckeditor">
                            {{ old('description') }}
                        </textarea> 
                    </div>

                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" name="category_id" id="category">
                            <option value="" selected="true">Select Category</option>
                            @foreach($categories as $category)
                                
                                <option value="{{$category->id}}" {{ old('category_id') == $category->id ? 'selected' : '' }}>{{$category->name}}</option>

                            @endforeach
                        </select>
                    </div>
                      
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" class="form-control" >
                    </div>

                    <div class="form-group">
                        <img src="{{ asset('uploads/product_images/default.png') }}" alt="" class="img-thumbnail" width="100">
                    </div>
                    
                    <div class="form-group">
                        <label>Initial Price</label>
                        <input type="number" name="initial_price" step="0.01" class="form-control"  value="{{ old('price') }}" >
                    </div>
                    
                    <div class="form-group">
                        <label>Sales Price</label>
                        <input type="number" name="sales_price" step="0.01" class="form-control"  value="{{ old('sales_price') }}" >
                    </div>
                    
                    <div class="form-group">
                        <label>Stock</label>
                      <input type="number" name="stock" class="form-control"  value="{{ old('stoke') }}" >
                    </div>
                    
                  </div>
                  <!-- /.card-body -->

                  
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create Product <i class="fa fa-plus"></i></button>
                </div>
            </form>
              </div>
            
        </div>
    </div>
        
</div>

</div><!-- Container end -->
    

@endsection

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
     
        $('#category').on('change',function(e) {
         
         var cat_id = e.target.value;

         console.log(cat_id);

         $.ajax({
               
               url:"{{ route('dashboard.products.create') }}",
               type:"POST",
               data: {
                   id: cat_id
                },
              
               success:function (data) {

                $('#subcategory').empty();

                $.each(data.subcategories[0].subcategories,function(index,subcategory){
                    
                    $('#subcategory').append('<option value="'+subcategory.id+'">'+subcategory.name+'</option>');
                })

               }
           })
        });

    });
</script>