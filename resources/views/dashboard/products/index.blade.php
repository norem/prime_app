@extends('layouts.dashboard.app')

@section('content')
<div class="container">
  


    <div class="content-header">
        <div class="container-fluid">
          
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Products</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Products</a></li>
                        <li class="breadcrumb-item active">Index</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="card">

        


        <div class="card-header" >
        <h3 class="card-title">All Products  -  {{ $products->total()}}</h3>
          @if (auth()->user()->hasPermission('create-products'))
            <a href="{{ route('dashboard.products.create') }}"  class="btn btn-primary float-right">Create a Product <i class="fa fa-plus "></i></a>
          @endif
      </div>
        
        <!-- /.card-header -->
        <div class="card-body">
        
        <form action="{{ route('dashboard.products.index')}}" method="get" class="mb-4">
          <div class="container">
            <div class="row">    
                <div class="col-md-12">
                <div class="input-group">
                        <div class="input-group-btn search-panel">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                              <span id="search_concept"><span class="fa fa-align-justify"></span> All</span>  <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              @foreach ($categories as $category)
                                <li><a href="{{ route('dashboard.products.index', ['category_id' => $category->id]) }}" name="category_id" >{{ $category->name }}</a></li>
                              @endforeach
                            </ul>
                        </div>        
                        <input type="text" class="form-control" name="product_search" placeholder="Search product..." value="{{ request()->product_search}}">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><span class="fa fa-search"></span></button>
                        </span>
                    </div>
                </div>
          </div>
        </div>

        </form>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th >#</th>
                <th >Product Name</th>
                <th >Product Description</th>
                <th >Category Name</th>
                <th >Product Image</th>
                <th >Unit Price</th>
                <th >In Stock</th>
                <th style="width: 130px">Actions</th>
              </tr>
            </thead>
            <tbody id="productsTable">
                @foreach ($products as $index=>$product)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $product->name}}</td>
                        <td>{!! $product->description !!}</td>
                        <td>{{ $product->category->name }}</td>
                        <td> <img src="{{ $product->image_path}}" class="img-thumbnail" alt="" width="100"> </td>
                        <td>{{ $product->sales_price}}</td>
                        <td>{{ $product->stock}}</td>
                        <td>
                          @if (auth()->user()->hasPermission('update-products'))
                            <a href="{{ route('dashboard.products.edit', $product->id) }}" class="btn btn-info btn-sm">Edit</a>
                          @endif
                        
                        <form action="{{ route('dashboard.products.destroy', $product->id) }}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                             @if (auth()->user()->hasPermission('delete-products'))
                              <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                             @endif
                            
                        </form>
                        </td>
                    </tr>
                @endforeach
              
            </tbody>
          </table>
        </div><!-- end of table responsive wrapper -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{$products->appends(request()->query())->links()}}
        </div>
      </div>

    
</div>
@endsection