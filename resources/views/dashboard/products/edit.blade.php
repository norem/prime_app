@extends('layouts.dashboard.app')

@section('content')
<div class="container">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Update Product Detail</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
                    <li class="breadcrumb-item"><a href="#">Products</a></li>
                    <li class="breadcrumb-item active">Update</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


<div class="container">
    <div class="row">
        
        <div class="col-10 offset-1">
            <div class="card card-primary">
  
                <!-- /.card-header -->
                <!-- form start -->
            <form action="{{ route('dashboard.products.update', $product->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')


                  <div class="card-body">
                    <div class="form-group">
                      <label>Name</label>
                    <input type="text" name="name" class="form-control"  value="{{ $product->name }}" required>
                    </div>
                    
                    <div class="form-group">
                        <label>Description</label>
                        <textarea name="description" class=" form-control ckeditor">
                            {{ $product->name }}
                        </textarea> 
                    </div>

                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" name="category_id" required>
                            <option value="" selected="true" >Select Category</option>
                            @foreach($categories as $category)
                                
                                <option value="{{$category->id}}" {{ $product->category_id == $category->id ? 'selected' : '' }}>{{$category->name}}</option>

                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" class="form-control" >
                    </div>

                    <div class="form-group">
                        <img src="{{ $product->image_path }}" alt="" class="img-thumbnail" width="100" width="100">
                    </div>

                    <div class="form-group">
                        <label>Price</label>
                        <input type="number" name="sales_price" step="0.01"  class="form-control"  value="{{ $product->price }}" required>
                    </div>

                    <div class="form-group">
                        <label>Stock</label>
                      <input type="number" name="stock" class="form-control"  value="{{ $product->stock }}" required>
                    </div>
                    
                  </div>
                  <!-- /.card-body -->

                  
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update Product <i class="fa fa-plus"></i></button>
                </div>
            </form>
              </div>
            
        </div>
    </div>
        
</div>

</div><!-- Container end -->
    

@endsection