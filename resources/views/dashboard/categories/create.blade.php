@extends('layouts.dashboard.app')

@section('content')
<div class="container">

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Create Category</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashbord</a></li>
                    <li class="breadcrumb-item"><a href="#">Categories</a></li>
                    <li class="breadcrumb-item active">Create</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>


<div class="container">
    <div class="row">
        
        <div class="col-10 offset-1">
            <div class="card card-primary">
  
                <!-- /.card-header -->
                <!-- form start -->
            <form action="{{ route('dashboard.categories.store') }}" method="post">
                @csrf
                @method('post')


                  <div class="card-body">
                    <div class="form-group">
                      <label>Name</label>
                    <input type="text" name="name" class="form-control"  value="{{ old('name') }}" >
                    </div>

                    <div class="form-group">
                      <label>Parent Category</label>
                      <select name="parent_id" class="form-control">
                          <option value="" selected>None</option>
                          @foreach ($parentCategories as $parent)
                            <option value="{{$parent->id}}">{{$parent->name}}</option>
                          @endforeach
                      </select>
                    </div>
                  </div>
                  <!-- /.card-body -->

                  
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Create Category <i class="fa fa-plus"></i></button>
                </div>
            </form>
              </div>
            
        </div>
    </div>
        
</div>

</div><!-- Container end -->
    

@endsection