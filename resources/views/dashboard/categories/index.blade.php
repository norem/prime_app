@extends('layouts.dashboard.app')

@section('content')
<div class="container">
  


    <div class="content-header">
        <div class="container-fluid">
          
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">categorys</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">categorys</a></li>
                        <li class="breadcrumb-item active">Index</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="card">

        


        <div class="card-header" >
        <h3 class="card-title">All Categories  -  {{ $categories->total()}}</h3>
          @if (auth()->user()->hasPermission('create-categories'))
            <a href="{{ route('dashboard.categories.create') }}"  class="btn btn-primary float-right">Create a Category <i class="fa fa-plus "></i></a>
          @endif
      </div>
        
        <!-- /.card-header -->
        <div class="card-body">
          <!-- Search form -->
          <form action="{{ route('dashboard.categories.index')}}" method="get" class="mb-4">
            <div class="input-group">
            <input type="text" class="form-control" name="category_search" value="{{ request()->category_search}}" placeholder="Looking for category!!">
              <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </div>
            </div>
          </form>
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">#</th>
                <th>Name</th>
                <th>Sub Category</th>
                <th>Products Count</th>
                <th>Related Products</th>
                <th style="width: 130px">Actions</th>
              </tr>
            </thead>
            <tbody id="categorysTable">
                @foreach ($categories as $index=>$category)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $category->name}}</td>
                        <td>
                          @foreach ($category->subCategory as $sub)
                            {{ $sub->name}}
                          @endforeach
                        </td>
                        <td>{{ $category->products->count() }}</td>
                        <td>
                          <a href="{{ route('dashboard.products.index', ['category_id' => $category->id])  }}" class="btn btn-info btn-sm">Show Related Products</a>
                        </td>
                        <td>
                          @if (auth()->user()->hasPermission('update-categories'))
                            <a href="{{ route('dashboard.categories.edit', $category->id) }}" class="btn btn-info btn-sm">Edit</a>
                          @endif
                        
                        <form action="{{ route('dashboard.categories.destroy', $category->id) }}" method="post" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                             @if (auth()->user()->hasPermission('delete-categories'))
                              <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                             @endif
                            
                        </form>
                        </td>
                    </tr>
                @endforeach
              
            </tbody>
          </table><!-- end of table -->
        </div><!-- end of table responsive wrapper -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
          {{$categories->appends(request()->query())->links()}}
        </div>
      </div>

    
</div>
@endsection