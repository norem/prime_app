<table id="example2" class="table table-bordered table-hover">
    <thead>
        <tr>
        <th>#</th>
        <th>Date</th>
        <th>Customer Name</th>
        <th>Gender</th>
        <th>Age Range</th>
        <th>Client Type</th>
        <th>Location</th>
        <th>Region</th>
        <th>District</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Product Purchased</th>
        <th>Total Amount</th>
        <th>Sold By</th>
        <th>Point of sale</th>
        <th>Payment Mode</th>
        </tr>
    </thead>
    <tbody>
        
    @foreach ($clients as $index=>$client)
        @foreach ($orders as $order)
            <tr>
                <td>{{$index+1}}</td>
                <td>{{\Carbon\Carbon::parse($client->created_at)->format('j/M/Y') }}</td>
                <td>{{$client->name}}</td>
                <td>{{$client->gender}}</td>
                <td>{{$client->age_group}}</td>
                <td>{{$client->client_type}}</td>
                <td>{{$client->place}}</td>
                <td>{{$client->region->name}}</td>
                <td>{{$client->district->name}}</td>
                <td>{{$client->phone}}</td>
                <td>{{$client->email}}</td>
                <td>
                @foreach ($order->products as $product)
                {{$product->name}}
                @endforeach
                </td>
                <td>{{$order->total_price}}</td>
                <td>{{$client->user->full_name}}</td>
                <td>{{$order->point_of_sale}}</td>
                <td>{{$order->payment_mode}}</td>
                
            </tr>
        @endforeach
    
    @endforeach
    
    </tbody>
    <tfoot>
    
    </tfoot>
</table>