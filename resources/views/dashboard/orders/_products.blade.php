
<div class="container">
   
    @foreach ($products as $product)
<div class="card">
	<div class="row">
	
		<aside class="col-sm-12">
<article class="card-body p-5">
	<h3 class="title mb-3">{{$product->name}}</h3>

<p class="price-detail-wrap"> 
	<span class="price h3 text-warning"> 
    <span class="currency">UGX</span><span class="num">{{ number_format($product->pivot->quantity * $product->unitprice,2)}}</span>
	</span> 
</p> <!-- price-detail-wrap .// -->
<dl class="item-property">
  <dt>Description</dt>
  <dd>{{$product->description}}</dd>
  <dd>Sold  to: {{$order->client->name}}</dd>

</dl>
<hr>
	<div class="row">
		<div class="col-sm-5">
			<dl class="param param-inline">
			  <dt>Quantity: </dt>
			  <dd>
			  	{{$product->pivot->quantity}}
			  </dd>
			</dl>  <!-- item-property .// -->
		</div> <!-- col.// -->
		
	</div> <!-- row.// -->
	</article> <!-- card-body.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</div> <!-- card.// -->
@endforeach

</div>
<!--container.//-->