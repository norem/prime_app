@extends('layouts.dashboard.app')

@section('content')
<div class="container">
  


    <div class="content-header">
        <div class="container-fluid">
          
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Orders</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="#">Orders</a></li>
                        <li class="breadcrumb-item active">Index</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="row">
      <div class="col-8">
        <div class="card">
          <div class="card-header" >
          <h3 class="card-title">All Orders  -  {{ $orders->total()}}</h3>
            {{-- @if (auth()->user()->hasPermission('create-orders'))
              <a href=""  class="btn btn-primary float-right">Create an Order <i class="fa fa-plus "></i></a>
            @endif --}}
        </div>
          
          <!-- /.card-header -->
          <div class="card-body">
            <!-- Search form -->
            <form action="{{ route('dashboard.orders.index')}}" method="get" class="mb-4">
              <div class="input-group">
              <input type="text" class="form-control" name="order_search" value="{{ request()->order_search}}" placeholder="Looking for an order!!">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
            @if ($orders->count() > 0)
              <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Client Name</th>
                      <th>Price</th>
                      <th>Created At</th>
                      <th>Order Products</th>
                      <th style="width: 130px">Actions</th>
                    </tr>
                  </thead>
                  
                  <tbody id="categorysTable">
                      @foreach ($orders as $index=>$order)
                          <tr>
                              <td>{{ $index + 1 }}</td>
                              <td>{{ $order->client->name}}</td>
                              <td>{{ number_format($order->total_price,2)}}</td>
                              <td>{{ $order->created_at->toFormattedDateString() }}</td>
                              <td><button class="btn btn-primary btn-sm order-products" 
                                data-url="{{ route('dashboard.orders.products', $order->id) }}" 
                                data-method="GET"><i class="fa fa-list"></i> Show Products</button></td>
                              <td>
                                {{-- @if (auth()->user()->hasPermission('update-orders'))
                                  <a href="{{ route('dashboard.clients.orders.edit', $order->id) }}" class="btn btn-info btn-sm">Edit</a>
                                @endif --}}
                              
                              <form action="{{ route('dashboard.orders.destroy', $order->id) }}" method="post" style="display: inline-block">
                                  @csrf
                                  @method('DELETE')
                                  @if (auth()->user()->hasPermission('delete-orders'))
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                  @endif
                                  
                              </form>
                              </td>
                          </tr>
                      @endforeach
                    
                  </tbody>
                </table><!-- end of table -->
              </div><!-- end of table responsive wrapper -->
              @else
              <h3>Sorry! no record found in your search.</h3>
            @endif
          
          </div>
          <!-- /.card-body -->
          <div class="card-footer clearfix">
            {{$orders->appends(request()->query())->links()}}
          </div>
    </div><!-- end of .card -->
      </div>
      <div class="col-4">
        <div id="order-products-show"></div>
      </div>
    </div>


    
</div><!-- end of .container -->
@endsection

@section('orderjs')
<script src="{{ asset('js/custom/order.js') }}"></script>
@endsection