@extends('layouts.frontend.app')

@section('content')
<!-- FEATURE -->
<section id="feature" data-stellar-background-ratio="0.5">
    <div class="container">
         <div class="row">

              <div class="col-md-12 col-sm-12">
                   <div class="section-title">
                        <h1>Get to know us</h1>
                   </div>
              </div>

              <div class="col-md-6 col-sm-6">
                   <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#tab01" aria-controls="tab01" role="tab" data-toggle="tab">VISION</a></li>

                        <li><a href="#tab02" aria-controls="tab02" role="tab" data-toggle="tab">MISSION</a></li>

                        <li><a href="#tab03" aria-controls="tab03" role="tab" data-toggle="tab">OUR OBJECTIVES</a></li>
                   </ul>

                   <div class="tab-content">
                        <div class="tab-pane active" id="tab01" role="tabpanel">
                             <div class="tab-pane-item">
                                  <h2>Our Vision</h2>
                                  <p>Safe Environment and clean habitat for present and future life.</p>
                             </div>
                             
                        </div>


                        <div class="tab-pane" id="tab02" role="tabpanel">
                             <div class="tab-pane-item">
                                  <h2>Our Mission</h2>
                                  <p>To sell and teach efficient natural resource management and to stop environment
                                    destruction and time wasting.</p>
                             </div>
                        </div>

                        <div class="tab-pane" id="tab03" role="tabpanel">
                            <div class="tab-pane-item">
                                <h2>Our Objectives</h2>
                                <p>
                                    <ul>
                                        <li>Creating awareness on the effects and causes of poor environmental management in the
                                            rural and urban areas in Uganda.</li>
                                        <li>To make the community aware of the effects of development on the environment.</li>
                                        <li>To teach Ugandans about environmentally friendly energy saving technologies.</li>
                                        <li>To teach Ugandans about kitchen and time management.</li>
                                        <li>To inform Ugandans about alternative sources of energy to fuel/firewood users, to
                                            enable them to have a better choice.</li>
                                        <li>To teach Ugandans the techniques of utilizing energy saving stoves/better end use of
                                            stoves.</li>
                                        <li>To encourage the community on the tree planting and tree care.</li>
                                        <li>To produce efficient, strong, and cost effective energy saving stoves for both rural and
                                            urban communities.</li>
                                        <li>To carryout research in energy saving technologies and be able to sell them to other
                                            parties.</li>
                                        <li>To work together with other parties through networking and advocacy, in spearheading
                                            a better environment for human settlement.</li>
                                    </ul>
                                </p>
                           </div>
                        </div>
                   </div>

              </div>
         </div>
    </div>
</section>

<!-- ABOUT -->
     <section id="about" data-stellar-background-ratio="0.5">
          <div class="container">
               <div class="row">

                    <div class="col-md-offset-3 col-md-6 col-sm-12">
                         <div class="section-title">
                              <h1>About us</h1>
                         </div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                         <div class="team-thumb">
                              <div class="team-info team-thumb-up">
                                   <p>PEES identified deforestation and inefficient management of natural resources, as a
                                    substantial threat to the health and wealth of many Ugandans, both in the rural and urban
                                    areas.</p>
                                   <p>At PEES, we believe in providing high standards of professional services and expertise.
                                    Our success is based on efficient and effective practices, and the capability to adapt to the
                                    changing needs of our clients. Additionally, we endeavor to ensure that our clients obtain
                                    maximum benefit from our services.</p>
                                   <p>At PEES, we uphold professional standards to keep pace with the rapidly changing
                                    commercial world. Our company directors and personnel undergo regular trainings, which
                                    are conducted both internally at company level, and also externally with the support of
                                    professional experts, through sectoral and professional development seminars/workshops.</p>
                                   
                                </div>
                         </div>
                    </div>
               </div>
          </div>
     </section>
    
@endsection