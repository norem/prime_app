<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
<a href="{{ route('dashboard.index') }}" class="brand-link">
      <img src="{{ asset('assets/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Prime Energy</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('uploads/usersImage/' . Auth::User()->avator) }}" class="img-circle " alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::User()->email }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="{{ route('dashboard.index')}}" class="nav-link {{ Route::is('dashboard.index') ? 'active' : '' }}">
              <i class="nav-icon fa fa-tachometer"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @if (auth()->user()->hasPermission('read-users'))
          <li class="nav-item">
            <a href="{{ route('dashboard.users.index')}}" class="nav-link {{ Route::is('dashboard.users.index') ? 'active' : '' }}">
                <i class="nav-icon fa fa-user-circle"></i>
                <p>
                 Users
                </p>
              </a>
            </li>              
          @endif

          @if (auth()->user()->hasPermission('read-categories'))
          <li class="nav-item">
            <a href="{{ route('dashboard.categories.index')}}" class="nav-link {{ Route::is('dashboard.categories.index') ? 'active' : '' }}">
                <i class="nav-icon fa fa-th"></i>
                <p>
                 Categories
                </p>
              </a>
            </li>              
          @endif

          @if (auth()->user()->hasPermission('read-products'))
          <li class="nav-item">
            <a href="{{ route('dashboard.products.index')}}" class="nav-link {{ Route::is('dashboard.products.index') ? 'active' : '' }}">
                <i class="nav-icon fa fa-product-hunt"></i>
                <p>
                 Products
                </p>
              </a>
            </li>              
          @endif

          @if (auth()->user()->hasPermission('read-clients'))
          <li class="nav-item">
            <a href="{{ route('dashboard.clients.index')}}" class="nav-link {{ Route::is('dashboard.clients.index') ? 'active' : '' }}">
                <i class="nav-icon fa fa fa-users"></i>
                <p>
                 Clients
                </p>
              </a>
            </li>              
          @endif

          @if (auth()->user()->hasPermission('read-orders'))
          <li class="nav-item">
            <a href="{{ route('dashboard.orders.index')}}" class="nav-link {{ Route::is('dashboard.orders.index') ? 'active' : '' }}">
                <i class="nav-icon fa fa-shopping-basket"></i>
                <p>
                 Orders
                </p>
              </a>
            </li>              
          @endif

          @if (auth()->user()->hasPermission('read-reports'))
          <li class="nav-item">
            <a href="{{ route('dashboard.reports.index')}}" class="nav-link {{ Route::is('dashboard.reports.index') ? 'active' : '' }}">
                <i class="nav-icon fa fa-flag"></i>
                <p>
                 Transaction Reports
                </p>
              </a>
            </li>              
          @endif

          @if (auth()->user()->hasPermission('read-audits'))
          <li class="nav-item">
            <a href="{{ route('dashboard.audits.index')}}" class="nav-link {{ Route::is('dashboard.audits.index') ? 'active' : '' }}">
                <i class="nav-icon fa fa-file"></i>
                <p>
                 Audits Report
                </p>
              </a>
            </li>              
          @endif

          
             
        
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>