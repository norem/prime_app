<!-- HERO -->
<section id="home" data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="container">
         <div class="row">

              <div class="col-md-offset-3 col-md-6 col-sm-12">
                   <div class="home-info">
                        <h1>Prime Energy and Environment Savers Limited</h1>
                        <h3>was established in 2004 and
                         registered in Uganda as a Certified Limited Company, by the Registrar of Companies.</h3>
                         <div>
                              Certificate of Registration No. 64400. <br>
                              Tax Identification Number (TIN): 1000151352 <br>
                              Value Added Tax Number (VAT No.): 46265-T <br>
                         </div>
                        <form action="" method="get" class="online-form">
                             <input type="email" name="email" class="form-control" placeholder="Enter your email" required>
                             <button type="submit" class="form-control">Subscribe</button>
                        </form>
                   </div>
              </div>

         </div>
    </div>
</section>