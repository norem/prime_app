@include('layouts.partials.dashboard._head_links')

@include('layouts.partials.dashboard._header')
<link rel="stylesheet" href="{{ asset('/assets/plugins/morris/morris.css') }}">
<div class="wrapper">

 @include('layouts.partials.dashboard._menu')
 @include('layouts.partials.dashboard._sidebar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('errors')
        <main class="py-4">
            @yield('content')
            
        </main>
    </div> <!-- /.content-wrapper -->
    <script src="/assets/plugins/ckeditor/ckeditor.js"></script>
    @include('layouts.partials.dashboard._footer')
</div>
<!-- ./wrapper -->

@include('layouts.partials.dashboard._scripts')
@yield('orderjs')
<script src='/assets/plugins/raphael/raphael.min.js'></script>
<script src='/assets/plugins/morris/morris.min.js'></script>
@yield('morris')

