<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Prime Energy</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>


    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" 
    rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{ asset('css/tooplate-style.css') }}">

    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
</head>
<body>
    @include('layouts.partials.frontend.menu');
    @include('layouts.partials.frontend.hero');
        <main>
            @yield('content')
        </main>
    @include('layouts.partials.frontend.contactform');
    @include('layouts.partials.frontend.footer');

     <!-- SCRIPTS -->
     <script src="{{ asset('js/custom/jquery.js') }}"></script>
     <script src="{{ asset('js/custom/bootstrap.min.js') }}"></script>
     <script src="{{ asset('js/custom/jquery.stellar.min.js') }}"></script>
     <script src="{{ asset('js/custom/owl.carousel.min.js') }}"></script>
     <script src="{{ asset('js/custom/smoothscroll.js') }}"></script>
     <script src="{{ asset('js/custom/custom.js') }}"></script>
</body>
</html>